'use strict';

const ENV = process.env.NODE_ENV || 'development';

require('dotenv-safe').load({ allowEmptyValues: true });

const cors = require('cors');
const express = require('express');
const morgan = require('morgan');
const methodOverride = require('method-override');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const logger = require('../utils/logger');
const helmet = require('helmet');
const config = require('./index');
const APIResponse = require('../lib/APIResponse');

// Initialize Express
const server = express();

logger.info('Initializing %s configs', 'Express');

// Load environment variables from .env
server.set('env', ENV);

const sessionOpts = {
  secret: config.session.secret,
  key: 'skey.sid',
  resave: config.session.resave,
  saveUninitialized: config.session.saveUninitialized
};

if (config.proxy.trust) {
  server.enable('trust proxy');
}

server.disable('x-powered-by');
server.use(helmet());
server.use(cors());
server.use(morgan('dev'));
server.use(methodOverride());
server.use(bodyParser.json({ limit: '20mb' }));
server.use(bodyParser.urlencoded({ extended: false }));
server.use(cookieParser());
server.set('json space', 0);

server.use(session(sessionOpts));

server.use(passport.initialize());
server.use(passport.session());

server.use(APIResponse.bind());

require('../lib/db');
require('../models');
require('../routes').init(server);

server.use((err, req, res, next) => {
  res.api.error(err);
});

module.exports = server;
