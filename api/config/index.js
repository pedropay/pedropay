'use strict';

const path = require('path');

module.exports = {
  all: {
    env: process.env.NODE_ENV || 'development',
    root: path.join(__dirname, '..'),
    port: process.env.PORT || 3000,
    ip: process.env.IP || '0.0.0.0',
    masterKey: process.env.MASTER_KEY,
    jwtSecret: process.env.JWT_SECRET
  },
  mysql: {
    host: process.env.MYSQL,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
  },
  mongo: {
    uri: process.env.DB
  },
  logger: {
    level: process.env.LOGGER_LEVEL || 'info',
    mongodb: {
      uri: process.env.DB,
      collection: process.env.DB_LOGS
    }
  },
  session: {
    type: 'mongo', // Store type, default `memory`
    secret: 'someVeRyN1c3S#cr3tHer34U',
    resave: false, // Save automatically to session store
    saveUninitialized: true // Saved new sessions
  },
  proxy: {
    trust: true
  },
  google: {
    geocode: 'AIzaSyAePwF07AeMYkmwuEq1lhBrvG5VH-31SjA',
    places: 'AIzaSyAefC3M-afxOrKcMcqBAj4cFMjPAUHjUcU'
  }
};
