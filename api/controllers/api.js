'use strict';

const Ctrl = {};

const grand = require('grand');
const faker = require('faker');
const models = require('../models');

Ctrl.index = async (req, res, next) => {
  return res.api.getOne({
    message: 'pedropay API'
  });
};

Ctrl.token = async (req, res, next) => {
  return res.api.getOne({
    message: 'Token',
    access_token: req.session.access_token
  });
};

module.exports = Ctrl;
