'use strict';

const models = require('../models');
const FBLib = require('../lib/FBLib');
const S = require('string');
const numeral = require('numeral');
const ub = require('../lib/ub');

const PAGE_ACCESS_TOKEN = 'EAACA9E7ZAoHgBAI8vtqZBTjNQZBaWszPtLPjEjxBccRALgmOEQqhbZBbhdHg66EHgZArR6jdCGoQAyZCdOJLvVTGmX98Ii7y1uBB6vTGZByY54DrBRZBCLcfWhAzD4Y0iS3uMzxPPBUSqCJARNDwn7cIBuAZAK6lcW9bUBG5HgMHsNQZDZD';

const Ctrl = {};

Ctrl.webhookGet = async (req, res, next) => {
  const challenge = await FBLib.Messenger.subscribe(req.query);
  return res.send(challenge);
};


Ctrl.webhookPost = async (req, res, next) => {
  console.log('webhookPost');

  if (
    req.body.entry &&
    req.body.entry.length > 0 &&
    req.body.entry[0].messaging &&
    req.body.entry[0].messaging.length > 0
  ) {
    const message = req.body.entry[0].messaging[0];

    const fbId = translateMessengerToFb(message.sender.id);
    const text = message.message.text;

    console.log('fbId', fbId);
    console.log('mId', message.sender.id);

    const sender = await models.User.findByFBID(fbId);

    if (sender) {
      try {
        const commandResult = await parseCommand(text, sender);
        console.log('ok');
        res.send('ok');
      } catch (e) {
        console.log('error: ' + e.message);
        res.send('error: ' + e.message);
      }
    } else {
      console.log('fail 2');
      res.send('fail 2');
    }
  } else {
    console.log('fail 1');
    res.send('fail 1');
  }
};

const parseCommand = async (text, sender) => {
  let cleanText = S(text).collapseWhitespace().s.toUpperCase();
  const textArr = cleanText.split(' ');

  await models.ChatLog.create({userId: sender.id, content: cleanText });

  console.log('CMD:', cleanText);

  if (textArr.length > 0) {
    const cmd = textArr[0];

    if (cmd === 'ID') {
      await FBLib.Messenger.send(PAGE_ACCESS_TOKEN, translateFbToMessenger(sender.fb_id), "Your PedroPay ID is\n\n" + sender.uid);
    } else if (cmd === 'PAY') {
      if (textArr.length !== 4) {
        throw new Error('MISSING_PARAMETERS');
      }

      const receiverUid = textArr[1];
      let amount        = parseFloat(textArr[2]);
      const pin         = textArr[3];

      if (isNaN(amount)) {
        amount = -1;
      }

      const receiver = await models.User.findByUID(receiverUid);

      if (!receiver) {
        throw new Error('INVALID_UID');
      }

      const isPinValid = await models.User.checkPin(sender.id, pin);

      if (!isPinValid) {
        throw new Error('INVALID_PIN');
      }

      if (amount <= 0) {
        throw new Error('INVALID_AMOUNT');
      }

      const balance = await ub.getAccountBalance(sender.uid);

      if (balance <= 0) {
        await models.Transaction.create({ senderId: sender.id, receiverId: receiver.id, amount: amount, status: 'INSUFFICIENT_FUNDS' });
        throw new Error('INSUFFICIENT_FUNDS');
      }

      await ub.transferFunds(sender.uid, {
        senderTransferId: "00001",
        transferRequestDate: new Date(),
        accountNo: receiver.ub_account_no,
        amount: {
          currency: "PHP",
          value: amount
        },
        remarks: 'pedropay',
        particulars: '',
        info: []
      });

      console.log('Transferring funds');
      let status = 'OK';
      const tid = await models.Transaction.create({ senderId: sender.id, receiverId: receiver.id, amount: amount, status: status });
      const trans = await models.Transaction.find(tid);

      await FBLib.Messenger.send(PAGE_ACCESS_TOKEN, translateFbToMessenger(sender.fb_id), "You transferred PHP " + numeral(amount).format('0,0.00') +" to " + receiver.name + ".\n Transaction No. " + trans.uid);
      await FBLib.Messenger.send(PAGE_ACCESS_TOKEN, translateFbToMessenger(receiver.fb_id), "You received PHP " + numeral(amount).format('0,0.00') +" from " + sender.name + ".\n Transaction No. " + trans.uid);
      return true;
    } else if (cmd === 'REFUND') {
      if (textArr.length !== 3) {
        throw new Error('MISSING_PARAMETERS');
      }

      const transactionUid = textArr[1];
      const pin            = textArr[2];

      const transaction = await models.Transaction.findByUid(transactionUid);

      if (!transaction) {
        throw new Error('INVALID_TXN_UID');
      }

      if (transaction.receiver_id !== sender.id) {
        throw new Error('USER_DOES_NOT_OWN_TXN');
      }

      const isPinValid = await models.User.checkPin(sender.id, pin);

      if (!isPinValid) {
        throw new Error('INVALID_PIN');
      }

      const balance = await ub.getAccountBalance(sender.uid);

      if (balance <= 0) {
        await models.Transaction.create({ senderId: sender.id, receiverId: receiver.id, amount: amount, status: 'INSUFFICIENT_FUNDS'});
        throw new Error('INSUFFICIENT_FUNDS');
      }

      const refundedUser = await models.User.find(transaction.sender_id);

      console.log({
        senderTransferId: "00001",
        transferRequestDate: new Date(),
        accountNo: refundedUser.ub_account_no,
        amount: {
          currency: "PHP",
          value: transaction.amount
        },
        remarks: 'pedropay',
        particulars: '',
        info: []
      });

      await ub.transferFunds(sender.uid, {
        senderTransferId: "00001",
        transferRequestDate: new Date(),
        accountNo: refundedUser.ub_account_no,
        amount: {
          currency: "PHP",
          value: transaction.amount
        },
        remarks: 'pedropay',
        particulars: '',
        info: []
      });
      await models.Transaction.create({ senderId: sender.id, receiverId: transaction.sender_id, amount: transaction.amount, status: 'OK'});

      await FBLib.Messenger.send(PAGE_ACCESS_TOKEN, translateFbToMessenger(sender.fb_id), "You refunded PHP " + numeral(transaction.amount).format('0,0.00') +" back to " + refundedUser.name + ".");
      await FBLib.Messenger.send(PAGE_ACCESS_TOKEN, translateFbToMessenger(refundedUser.fb_id), "You were refunded PHP " + numeral(transaction.amount).format('0,0.00') +" from " + sender.name + ".");
    } else if (cmd === 'NOTE') {
      if (textArr.length !== 3) {
        throw new Error('MISSING_PARAMETERS');
      }

      const transactionUid = textArr[1];
      const content        = textArr[2];

      const transaction = await models.Transaction.findByUid(transactionUid);

      if (!transaction) {
        throw new Error('INVALID_TXN_UID');
      }

      if (!transaction.sender_id !== sender.id && transaction.receiver_id !== sender.id) {
        throw new Error('USER_DOES_NOT_OWN_TXN');
      }

      await models.TxnNote.create({ userId: sender.id, transactionId: transaction.id, content: content });
    } else if (cmd === 'BAL' || cmd === 'BALANCE') {
      try {
        const balance = await ub.getAccountBalance(sender.uid);
        await FBLib.Messenger.send(PAGE_ACCESS_TOKEN, translateFbToMessenger(sender.fb_id), "Your current balance is:\n PHP " + numeral(balance).format('0,0.00'));
      } catch (e) {
        console.log('err-2', e);
      }

    } else if (cmd === 'HELP') {
      let helpText = '';
      helpText += "Hi, I'm Pedro! You can use the phrases below to transact with merchants.\n";
      helpText += "\n";
      helpText += "Get your ID\n";
      helpText += "ID\n";
      helpText += "\n";
      helpText += "Pay a merchant\n";
      helpText += "PAY <pedro_id> <amount> <pin>\n";
      helpText += "\n";
      // helpText += "Add a note to a transaction\n";
      // helpText += "NOTE <transaction_id> <text>\n";
      // helpText += "\n";
      helpText += "Refund a transaction back to the sender\n";
      helpText += "REFUND <transaction_id> <pin>\n";
      helpText += "\n";
      helpText += "Check balance\n";
      helpText += "BAL\n";
      helpText += "\n";
      helpText += "Get a list of chat commands\n";
      helpText += "HELP\n";
      helpText += "\n";
      helpText += "* All phrases are case insensitive\n";
      await FBLib.Messenger.send(PAGE_ACCESS_TOKEN, translateFbToMessenger(sender.fb_id), helpText);
    }
  }
};

const translateMessengerToFb = (id) => {
  const arr = {
    '1846682768683364': '1846682768683364',
    '1554433021319748': '10155281277083403',
    '1376249565817704': '10155243513910945',
    '1944242635592845': '4883792245705',
  };

  return arr[id];
};

const translateFbToMessenger = (id) => {
  const arr = {
    '1846682768683364': '1846682768683364',
    '10155281277083403': '1554433021319748',
    '10155243513910945': '1376249565817704',
    '4883792245705': '1944242635592845',
  };

  return arr[id];
};

module.exports = Ctrl;