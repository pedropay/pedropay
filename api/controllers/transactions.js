'use strict';

const models = require('../models');

const Ctrl = {};

Ctrl.findById = async (req, res, next) => {
  try {
    const transaction = await models.Transaction.find(req.params.id);

    return res.api.getOne(transaction);
  } catch (e) {
    return next(e);
  }
};

Ctrl.getBySenderId = async (req, res, next) => {
  try {
    const transactions = await models.Transaction.getByUserId(req.params.sender_id);

    return res.api.getMany(transactions);
  } catch (e) {
    return next(e);
  }
};

Ctrl.getByReceiverId = async (req, res, next) => {
  try {
    const transactions = await models.Transaction.getByUserId(req.params.receiver_id);

    return res.api.getMany(transactions);
  } catch (e) {
    return next(e);
  }
};

Ctrl.getByUserId = async (req, res, next) => {
  try {
    const transactions = await models.Transaction.getByUserId(req.params.user_id);

    return res.api.getMany(transactions);
  } catch (e) {
    return next(e);
  }
};

Ctrl.getTxnNotes = async (req, res, next) => {
  try {
    const notes = await models.TxnNote.getByTransactionId(req.params.id);

    return res.api.getMany(notes);
  } catch (e) {
    return next(e);
  }
};

module.exports = Ctrl;