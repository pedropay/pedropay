'use strict';

const constants = {
  accountInfoUrl    : 'https://api-uat.unionbankph.com/partners/sb/accounts',
  accountBalanceUrl : 'https://api-uat.unionbankph.com/partners/sb/accounts/v1/balances',
  authUrl           : 'https://api-uat.unionbankph.com/partners/sb/convergent/v1/oauth2/authorize',
  tokenUrl          : 'https://api-uat.unionbankph.com/partners/sb/convergent/v1/oauth2/token',
  redirectUri       : 'https://api.pedropay.com/ub/redirect',
  url               : 'https://api-uat.unionbankph.com/partners/sb/online/v1/transfers/single',
  id                : 'e88b79d7-7b6a-4144-a74d-0b1059b5f727',
  secret            : 'sJ5hK7lY0rX4cE7bU1cM2qF3aA7cR5jD2uV3aG6pA4yW6pP0uG',
  token: 'AAEkZTg4Yjc5ZDctN2I2YS00MTQ0LWE3NGQtMGIxMDU5YjVmNzI3MR24HNJJMtCkXduYrB0AYWXfEYkj3stIaFxAoekIzQ8cIzAYqPowMjVTDyvXYYT2KNsT78WbxuEhP7y89wL_LKugTGrF1WvUXct-nAeD-8sLizivffHxTwg9eHwdr3YOW-xAHGw_OtRijIXjfWpen8jpo4RBcO-IUuGfxT2DXEWuogkAn7kt8rxGGspKBLbFiJDvu-F18C2v5515oBAGTvfpMSKBXnFONfxuup9F8IMVchMj0PFLEAdyyw8YobqLcA99eNMaeEdrRAHlLHdIdLnHfN6_QTmpFruNWKvueFF1gncCllwKBmDfi43hAX2TC_MCsrniXYArllaLmsaA2o1LjXO-LOTHNmBez8lxZkY'
};

const request = require('superagent');
const grand = require('grand');
const faker = require('faker');
const models = require('../models');

const Ctrl = {};
/*
-> auth = id, redirect_uri,scope,response_type=code
-> token
    -H -> content-type -> urlencoded
    -H -> accept text/html
    -D -> code, redirect_uri, client_id, grant_type=authorization_code */

Ctrl.init = async (req, res, next) => {
  if (req.params.uid) req.session.uid = req.params.uid;
  res.redirect('/ub/auth');
};

Ctrl.auth = async (req, res, next) => {
  let params = 'client_id=' + constants.id + '&redirect_uri=' + encodeURIComponent(constants.redirectUri) +
    '&scope=balances%20payments%20transfers&response_type=code';

  const auth = await request.get(constants.authUrl + '?' + params);

  try {
    return res.redirect(auth.redirects[0]);
  } catch (e) {
    return next(e);
  }
};

Ctrl.redirect = async (req, res, next) => {
  const token = await request.post(constants.tokenUrl)
    .send('code=' + req.query.code)
    .send('redirect_uri=' + constants.redirectUri)
    .send('client_id=' + constants.id)
    .send('grant_type=authorization_code');

  let bodyToken = {
    ubAccountNo: '103267282483',
    ubAccessToken: token.body.access_token,
    ubTokenExpires: token.body.expires_in
  };

  try {
    await models.User.saveUBToken(Object.assign({ id: req.session.uid }, bodyToken));

    return res.redirect('https://pedropay.com/register?ub_auth=1');
  } catch (e) {
    return next(e);
  }
};

Ctrl.getAccountInformation = async (req, res, next) => {
  const user = await models.User.getAccountNumber(req.params.uid);
  try {
    const account = await request.get(constants.accountInfoUrl + '/' + user)
      .set('x-ibm-client-id', constants.id)
      .set('x-ibm-client-secret', constants.secret);

    return res.api.getOne(account);
  } catch (e) {
    return next(e);
  }
};

Ctrl.getAccountBalance = async (req, res, next) => {
  const user = await models.User.getUBAccessToken(req.params.uid);

  try {
    const account = await request.get(constants.accountBalanceUrl)
      .set('accept', 'application/json')
      .set('content-type', 'application/json')
      .set('authorization', 'Bearer ' + user.ub_access_token)
      .set('x-ibm-client-id', constants.id)
      .set('x-ibm-client-secret', constants.secret);

    console.log(JSON.parse(account.res.text)[0].amount);
    return res.api.getOne(JSON.parse(account.res.text)[0].amount);
  } catch (e) {
    next(e);
  }
};

Ctrl.transferFunds = async (req, res, next) => {
  const token = await models.User.getUBAccessToken(req.params.uid);

  try {
    await request
      .post(constants.url)
      .set('accept-type', 'application/json')
      .set('content-type', 'application/json')
      .set('authorization', 'Bearer ' + token)
      .set('x-ibm-client-id', constants.id)
      .set('x-ibm-client-secret', constants.secret)
      .send(req.body);

    return res.api.successMessage('OK');
  } catch (e) {
    return next(e);
  }
};

Ctrl.dummy = async (req, res, next) => {

  try {
    for (let i = 0; i < 100; i++) {
      await models.User.create({
        email: grand.emailAddress(),
        name: grand.name(),
        gender: grand.gender(),
        city: faker.address.city(),
        age: grand.number(40)
      });
    }

    return next();
  } catch (e) {
    throw e;
  }
};

module.exports = Ctrl;
