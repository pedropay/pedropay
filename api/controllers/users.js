'use strict';

const models = require('../models');

const Ctrl = {};

Ctrl.findById = async (req, res, next) => {
  try {
    const user = await models.User.find(req.params.id);

    return res.api.getOne(cleanUser(user));
  } catch (e) {
    return next(e);
  }
};

Ctrl.findByUID = async (req, res, next) => {
  try {
    const user = await models.User.findByUID(req.query.uid);

    return res.api.getOne(cleanUser(user));
  } catch (e) {
    return next(e);
  }
};

Ctrl.findByEmail = async (req, res, next) => {
  try {
    const user = await models.User.findByEmail(req.query.email);

    return res.api.getOne(cleanUser(user));
  } catch (e) {
    return next(e);
  }
};

Ctrl.findByFBID = async (req, res, next) => {
  try {
    const user = await models.User.findByFBID(req.query.fb_id);

    return res.api.getOne(cleanUser(user));
  } catch (e) {
    return next(e);
  }
};

Ctrl.create = async (req, res, next) => {
  try {
    let user = null;

    user = await models.User.findByEmail(req.body.email);

    if (user) {
      return next(new Error('Email already used'));
    }

    user = await models.User.findByFBID(req.body.fb_id);

    if (!user) {
      const userId = await models.User.create({
        email          : req.body.email,
        name           : req.body.name,
        fbId           : req.body.fb_id,
        fbAccessToken  : req.body.fb_access_token,
        fbTokenExpires : req.body.fb_token_expires,
      });
      user = await models.User.find(userId);
      user.is_new = true;
    } else {
      user.is_new = false;
    }

    return res.api.create(cleanUser(user));
  } catch (e) {
    return next(e);
  }
};

Ctrl.saveFBToken = async (req, res, next) => {
  try {
    await models.User.saveFBToken({
      id             : req.params.id,
      fbId           : req.body.fb_id,
      fbAccessToken : req.body.fb_access_token,
      fbTokenExpires : req.body.fb_token_expires,
    });

    return res.api.successMessage('OK');
  } catch (e) {
    return next(e);
  }
};

Ctrl.saveUBToken = async (req, res, next) => {
  try {
    await models.User.saveUBToken(Object.assign(req.params, req.body));
    await models.User.saveUBToken({
      id             : req.params.id,
      ubAccountNo    : req.body.ub_account_no,
      ubAccessToken : req.body.ub_access_token,
      ubTokenExpires : req.body.ub_token_expires
    });

    return res.api.successMessage('OK');
  } catch (e) {
    return next(e);
  }
};

Ctrl.updatePin = async (req, res, next) => {
  try {
    await models.User.updatePin(req.params.id, req.body.pin);

    return res.api.successMessage('OK');
  } catch (e) {
    return next(e);
  }
};

Ctrl.update = async (req, res, next) => {
  try {
    await models.User.update({
      id: req.params.id,
      email: req.body.email,
      name: req.body.name,
      pin: req.body.pin,
    });

    return res.api.successMessage('OK');
  } catch (e) {
    return next(e);
  }
};

const cleanUser = (user) => {
  if (!user)
    return user;

  delete user.pin;
  delete user.fb_id;
  delete user.ub_account_no;
  delete user.created_at;
  return user;
};

module.exports = Ctrl;