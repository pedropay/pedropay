class APIResponse {
    static bind () {
        return (req, res, next) => {
            res.api = new APIResponse(res);
            next();
        };
    }

    constructor(res) {
        this.res = res;

        this.getMany          = this.getMany.bind(this);
        this.getOne           = this.getOne.bind(this);
        this.getOneOrNotFound = this.getOneOrNotFound.bind(this);
        this.successMessage   = this.successMessage.bind(this);
        this.create           = this.create.bind(this);
        this.notFound         = this.notFound.bind(this);
        this.unauthorized     = this.unauthorized.bind(this);
        this.error            = this.error.bind(this);
    }

    getMany(data) {
        this.res
            .status(200)
            .json({
                "result": APIResponse.ResultCode.OK,
                "data": data,
                "count": data.length,
            });
    }

    getOne(d) {
        let data = (Array.isArray(d)) ? d[0] : d;

        this.res
            .status(200)
            .json({
                "result" : APIResponse.ResultCode.OK,
                "data"   : data || null,
                "count"  : (data) ? 1 : 0,
            });
    }

    getOneOrNotFound(d) {
        console.log(this);
        if (d) {
            return this.res.api.getOne(d);
        } else {
            return this.res.api.notFound();
        }
    }

    successMessage(text) {
        this.getOne({
            "result": APIResponse.ResultCode.OK,
            "message": text
        });
    }

    create(d) {
        let data = (Array.isArray(d)) ? d[0] : d;

        this.res
            .status(201)
            .json({
                "result" : APIResponse.ResultCode.OK,
                "data"   : data || null,
                "count"  : (data) ? 1 : 0,
            });
    }

    notFound() {
        this.res
            .status(404)
            .json({
                "result": APIResponse.ResultCode.NOT_FOUND,
                "data": null,
                "count": 0,
            });
    }

    unauthorized() {
        this.res
            .status(401)
            .json({
                "result" : APIResponse.ResultCode.UNAUTHORIZED
            });
    }

    error(err) {
        this.res
            .status(500)
            .json({
                "result"  : APIResponse.ResultCode.INTERNAL_SERVER_ERROR,
                "message" : err.message,
                "stack"   : (err.stack) ? err.stack.split("\n") : undefined,
                "errors"  : err.errors || undefined
            });
    }
}

APIResponse.ResultCode = {
    OK: "OK",
    NOT_FOUND: "NOT_FOUND",
    UNAUTHORIZED: "UNAUTHORIZED",
    INTERNAL_SERVER_ERROR: "INTERNAL_SERVER_ERROR"
};

module.exports = APIResponse;
