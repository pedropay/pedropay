const {
  Facebook,
  FacebookApiException,
} = require('fb');

const FB_API_VERSION = 'v2.10';
const FB_APP_ID = '141786783260792';
const FB_APP_SECRET = '995e197d108c02988af1fbe9b068c228';
const FB_VERIFY_TOKEN = 'f072730717a9746258c89be6e95ae70f60627dc23d6a3d6035cd90bbfc535ebfeca2b917b3f42b91fea0bc9311d78fca';

const FBLib = {
  Messenger: {},
  User: {},
};

FBLib.Messenger.subscribe = (params) => {
  if (params['hub.mode'] === 'subscribe') {
    if (params['hub.verify_token'] === FB_VERIFY_TOKEN) {
      return params['hub.challenge'];
    } else {
      throw new Error('INVALID_FB_VERIFY_TOKEN');
    }
  } else {
    throw new Error('UNKNOWN_FB_HUB_MODE');
  }
};

FBLib.Messenger.send = async (pageAccessToken, recipientId, text) => {
  try {
    const fb = new Facebook({
      version     : FB_API_VERSION,
      appId       : FB_APP_ID,
      appSecret   : FB_APP_SECRET,
      accessToken : pageAccessToken,
    });

    const body = {
      recipient: {
        id: recipientId
      },
      message: {
        text: text
      }
    };

    const result = await fb.api('me/messages', 'post', body);

    if (!result || result.error) {
      throw new Error(result.error);
    }

    return result;
  } catch (err) {
    throw err;
  }
};

FBLib.User.find = async (userId) => {
  try {
    const fb = new Facebook({
      version     : FB_API_VERSION,
      appId       : FB_APP_ID,
      appSecret   : FB_APP_SECRET,
    });

    const resp = await fb.api('oauth/access_token', {
      client_id: FB_APP_ID,
      client_secret: FB_APP_SECRET,
      grant_type: 'client_credentials'
    });

    if (!resp.access_token) {
      throw NO_FB_ACCESS_TOKEN;
    }

    fb.setAccessToken(resp.access_token);

    const result = await fb.api(userId);

    if (!result || result.error) {
      throw new Error(result.error);
    }

    return result;
  } catch (err) {
    throw err;
  }

};

module.exports = FBLib;