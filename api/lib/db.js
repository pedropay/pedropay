const config = require('../config');
var db = require('mysql2-promise')();

db.configure({
  host     : config.mysql.host,
  user     : config.mysql.user,
  password : config.mysql.password,
  database : config.mysql.database,
});

module.exports = db;