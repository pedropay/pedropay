'use strict';

const models = require('../models');
const request = require('superagent');

const constants = {
  transferUrl: 'https://api-uat.unionbankph.com/partners/sb/online/v1/transfers/single',
  accountBalanceUrl: 'https://api-uat.unionbankph.com/partners/sb/accounts/v1/balances',
  id: 'e88b79d7-7b6a-4144-a74d-0b1059b5f727',
  secret: 'sJ5hK7lY0rX4cE7bU1cM2qF3aA7cR5jD2uV3aG6pA4yW6pP0uG'
};

const ub = {};

ub.transferFunds = async (uid, data) => {
  const token = await models.User.getUBAccessToken(uid);

  /** data
   *
   * {
   *  accountNo: data.accountNo,
   *  amount: {
   *    currency: data.currency,
   *    value: data.value
   *  },
   *  remarks: data.remarks
   * }
   *
   */

  try {
    await request
      .post(constants.transferUrl)
      .set('accept-type', 'application/json')
      .set('content-type', 'application/json')
      .set('authorization', 'Bearer ' + token.ub_access_token)
      .set('x-ibm-client-id', constants.id)
      .set('x-ibm-client-secret', constants.secret)
      .send(data);

    return true;
  } catch (e) {
    throw e;
  }
};

ub.getAccountBalance = async (uid) => {
  const user = await models.User.getUBAccessToken(uid);

  try {
    const balance = await request.get(constants.accountBalanceUrl)
      .set('accept', 'application/json')
      .set('content-type', 'application/json')
      .set('authorization', 'Bearer ' + user.ub_access_token)
      .set('x-ibm-client-id', constants.id)
      .set('x-ibm-client-secret', constants.secret);

    return JSON.parse(balance.res.text)[0].amount;
  } catch (e) {
    throw e;
  }
};

module.exports = ub;
