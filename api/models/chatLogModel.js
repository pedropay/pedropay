'use strict';

const db = require('../lib/db');

const Model = {};

Model.create = async ({ userId, content }) => {
  try {
    let chatLog = {
      user_id: userId,
      content: content,
      created_at: new Date(),
    };

    const [results, fields] = await db.query('INSERT INTO chat_logs SET ?', chatLog);

    return results.insertId;
  } catch (e) {
    throw e;
  }
};

module.exports = Model;