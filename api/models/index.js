'use strict';

const logger = require('../utils/logger');

logger.debug('Initializing %s configs', 'Models');

module.exports = {
  ChatLog: require('./chatLogModel'),
  Transaction: require('./transactionModel'),
  User: require('./userModel'),
  TxnNote: require('./txnNoteModel')
};
