'use strict';

const Hashids = require('hashids');
const S = require('string');
const db = require('../lib/db');

const Model = {};

Model.find = async (id) => {
  try {
    const [rows, fields] = await db.query('SELECT * FROM transactions WHERE id = ?;', [ id ]);

    if (rows && rows.length > 0) {
      return rows[0];
    } else {
      return null;
    }
  } catch (e) {
    throw e;
  }
};

Model.findByUid = async (uid) => {
  try {
    const [rows, fields] = await db.query('SELECT * FROM transactions WHERE uid = ?;', [ uid ]);

    if (rows && rows.length > 0) {
      return rows[0];
    } else {
      return null;
    }
  } catch (e) {
    throw e;
  }
};

Model.getBySenderId = async (userId) => {
  try {
    const [rows, fields] = await db.query('SELECT t.*, (SELECT u1.name FROM users u1 WHERE u1.id = t.sender_id) AS `sender_name`, (SELECT u1.name FROM users u1 WHERE u1.id = t.receiver_id) AS `receiver_name` FROM transactions t WHERE t.sender_id = ? ORDER BY t.created_at DESC', [ userId ]);

    return rows;
  } catch (e) {
    throw e;
  }
};

Model.getByReceiverId = async (userId) => {
  try {
    const [rows, fields] = await db.query('SELECT t.*, (SELECT u1.name FROM users u1 WHERE u1.id = t.sender_id) AS `sender_name`, (SELECT u1.name FROM users u1 WHERE u1.id = t.receiver_id) AS `receiver_name` FROM transactions t WHERE t.receiver_id = ? ORDER BY t.created_at DESC', [ userId ]);

    return rows;
  } catch (e) {
    throw e;
  }
};

Model.getByUserId = async (userId) => {
  try {
    const [rows, fields] = await db.query('SELECT t.*, (SELECT u1.name FROM users u1 WHERE u1.id = t.sender_id) AS `sender_name`, (SELECT u1.name FROM users u1 WHERE u1.id = t.receiver_id) AS `receiver_name` FROM transactions t WHERE t.sender_id = ? OR t.receiver_id = ? ORDER BY t.created_at DESC', [ userId, userId]);

    return rows;
  } catch (e) {
    throw e;
  }
};

Model.create = async ({ senderId, receiverId, amount, status, refundRefId = null }) => {
  try {
    let txn = {
      sender_id: senderId,
      receiver_id: receiverId,
      amount: amount,
      status: status,
      created_at: new Date()
    };

    if (refundRefId) {
      txn.refund_ref_id = refundRefId;
    }

    const [results, fields] = await db.query('INSERT INTO transactions SET ?', txn);

    const id = results.insertId;

    const hashids = new Hashids('04ecbf53c22f91f8586396334dd0159dcd1178fc', 0, 'ABCDEFHJKLM12345'); // all lowercase
    const uid = S(hashids.encode(id)).padLeft(8, '8').s;

    await db.query('UPDATE transactions SET uid = ? WHERE id = ?', [ uid, id ]);

    return id;
  } catch (e) {
    throw e;
  }
};

module.exports = Model;
