'use strict';

const db = require('../lib/db');

const Model = {};

Model.create = async ({ userId, transactionId, content }) => {
  try {
    let note = {
      user_id: userId,
      transaction_id: transactionId,
      content: content,
      created_at: new Date(),
    };

    const [results, fields] = await db.query('INSERT INTO txn_notes SET ?', note);

    return results.insertId;
  } catch (e) {
    throw e;
  }
};

Model.getByTransactionId = async (transactionId) => {
  try {
    const [rows, fields] = await db.query('SELECT * FROM txn_notes WHERE transaction_id = ? ORDER BY created_at DESC;', [ transactionId ]);

    return rows;
  } catch (e) {
    throw e;
  }
};

module.exports = Model;