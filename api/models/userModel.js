const db = require('../lib/db');
const sha1 = require('sha1');
const Hashids = require('hashids');
const S = require('string');

const Model = {};

Model.find = async (id) => {
  try {
    const [rows, fields] = await db.query('SELECT * FROM users WHERE id = ?', [id]);

    if (rows && rows.length > 0) {
      return rows[0];
    } else {
      return null;
    }
  } catch (e) {
    throw e;
  }
};

Model.findByUID = async (uid) => {
  try {
    const [rows, fields] = await db.query('SELECT * FROM users WHERE uid = ?', [uid]);

    if (rows && rows.length > 0) {
      return rows[0];
    } else {
      return null;
    }
  } catch (e) {
    throw e;
  }
};

Model.findByEmail = async (email) => {
  try {
    const [rows, fields] = await db.query('SELECT * FROM users WHERE email = ?', [email]);

    if (rows && rows.length > 0) {
      return rows[0];
    } else {
      return null;
    }
  } catch (e) {
    throw e;
  }
};

Model.findByFBID = async (fbId) => {
  try {
    const [rows, fields] = await db.query('SELECT * FROM users WHERE fb_id = ?', [fbId]);

    if (rows && rows.length > 0) {
      return rows[0];
    } else {
      return null;
    }
  } catch (e) {
    throw e;
  }
};

// UnionBank

Model.getAccountNumber = async (uid) => {
  try {
    const [rows, fields] = await db.query('SELECT ub_account_no FROM users WHERE uid = ?', [uid]);

    if (rows && rows.length > 0) {
      return rows[0];
    } else {
      return null;
    }
  } catch (e) {
    throw e;
  }
};

Model.getUBAccessToken = async (uid) => {
  try {
    const [rows, fields] = await db.query('SELECT ub_access_token FROM users WHERE uid = ?', [uid]);

    if (rows && rows.length > 0) {
      return rows[0];
    } else {
      return null;
    }
  } catch (e) {
    throw e;
  }
};

Model.create = async ({ email, name, gender, city, age, fbId = null, fbAccessToken = null, fbTokenExpires = null }) => {
  try {
    let user = {
      email: email,
      name: name,
      gender: gender,
      city: city,
      age: age,
      created_at: new Date()
    };

    if (fbId) {
      user.fb_id = fbId;
    }

    if (fbAccessToken) {
      user.fb_access_token = fbAccessToken;
    }

    if (fbTokenExpires) {
      user.fb_token_expires = fbTokenExpires;
    }

    const [results0, fields0] = await db.query('INSERT INTO users SET ?', user);
    const id = results0.insertId;

    let uid = email.toUpperCase().replace(/[^A-Z0-9]/g, '').substring(0, 8);

    const [rows, fields] = await db.query('SELECT id FROM users WHERE uid = ? LIMIT 1;', [ uid ]);

    if (rows.length > 0) {
      uid = uid.substring(0, 7) + '2';

      const [rows2, fields2] = await db.query('SELECT id FROM users WHERE uid = ? LIMIT 1;', [ uid ]);

      if (rows2.length > 0) {
        const hashids = new Hashids('04ecbf53c22f91f8586396334dd0159dcd1178fc', 0, 'ABCDEFHJKLM12345'); // all lowercase
        uid = S(hashids.encode(id)).padLeft(8, '8').s;
      }
    }

    await db.query('UPDATE users SET uid = ? WHERE id = ?', [ uid, id ]);

    return id;
  } catch (e) {
    throw e;
  }
};

Model.saveFBToken = async ({ id, fbId, fbAccessToken, fbTokenExpires }) => {
  try {
    await db.query('UPDATE users SET fb_id = ?, fb_access_token = ?, fb_token_expires = ? WHERE id = ?', [fbId, fbAccessToken, fbTokenExpires, id]);

    return true;
  } catch (e) {
    throw e;
  }
};

Model.saveUBToken = async ({ id, ubAccountNo, ubAccessToken, ubTokenExpires }) => {
  try {
    await db.query('UPDATE users SET ub_account_no = ?, ub_access_token = ?, ub_token_expires = ? WHERE id = ?', [ubAccountNo, ubAccessToken, ubTokenExpires, id]);

    return true;
  } catch (e) {
    throw e;
  }
};

Model.updatePin = async (id, pin) => {
  try {
    // TODO: PIN hashing using only sha1 for easy debugging
    const pinHash = sha1(pin);
    await db.query('UPDATE users SET pin = ? WHERE id = ?', [pinHash, id]);

    return true;
  } catch (e) {
    throw e;
  }
};

Model.checkPin = async (id, pin) => {
  try {
    const pinHash = sha1(pin);
    const [rows, fields] = await db.query('SELECT id FROM users WHERE id = ? AND pin = ? LIMIT 1;', [id, pinHash]);

    return (rows && rows.length > 0);
  } catch (e) {
    throw e;
  }
};

Model.update = async ({ id, email, name, pin }) => {
  try {
    // TODO: PIN hashing using only sha1 for easy debugging
    const pinHash = sha1(pin);
    await db.query('UPDATE users SET email = ?, name = ?, pin = ? WHERE id = ?', [email, name, pinHash, id]);

    return true;
  } catch (e) {
    throw e;
  }
};

module.exports = Model;
