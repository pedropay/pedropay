'use strict';

const express = require('express');
const router = express.Router();
const api = require('../controllers/api');

router.get('/', api.index);
router.get('/token', api.token);

module.exports = router;
