'use strict';

const express = require('express');
const router = express.Router();
const fb = require('../controllers/fb');

router.get('/webhook', fb.webhookGet);
router.post('/webhook', fb.webhookPost);

module.exports = router;

