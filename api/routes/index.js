'use strict';

const logger = require('../utils/logger');

module.exports.init = initRoutes;

function initRoutes (app) {
  logger.info('Initializing %s configs', 'Routes');

  let routesPath = __dirname;

  app.use('/ub', require(routesPath + '/ub'));
  app.use('/fb', require(routesPath + '/fb'));
  app.use('/transactions', require(routesPath + '/transactions'));
  app.use('/users', require(routesPath + '/users'));
  app.use('/api', require(routesPath + '/api'));
}
