'use strict';

const express = require('express');
const router = express.Router();
const transactions = require('../controllers/transactions');

router.get('/sender/:sender_id', transactions.getBySenderId);
router.get('/receiver/:receiver_id', transactions.getByReceiverId);
router.get('/user/:user_id', transactions.getByUserId);
router.get('/:id/notes', transactions.getTxnNotes);
router.get('/:id', transactions.findById);

module.exports = router;
