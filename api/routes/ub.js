'use strict';

const express = require('express');
const router = express.Router();
const ub = require('../controllers/ub');

router.get('/init/:uid', ub.init);
router.get('/auth', ub.auth);
router.get('/redirect', ub.redirect);
router.get('/accountinfo', ub.getAccountInformation);
router.get('/accountbal/:uid', ub.getAccountBalance);
router.post('/transferfunds/:uid', ub.transferFunds);

// dummy account
router.get('/dummy', ub.dummy);

module.exports = router;
