'use strict';

const express = require('express');
const router = express.Router();
const users = require('../controllers/users');

router.get('/find-by-uid', users.findByUID);
router.get('/find-by-email', users.findByEmail);
router.get('/find-by-fbid', users.findByFBID);
router.put('/:id/fb-token', users.saveFBToken);
router.put('/:id/ub-token', users.saveUBToken);
router.put('/:id/update-pin', users.updatePin);
router.get('/:id', users.findById);
router.put('/:id', users.update);
router.post('/', users.create);

module.exports = router;
