'use strict';

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const server = require('../bin/www');
const url = '/api/v1';

chai.use(chaiHttp);

it('should get all users', (done) => {
  chai.request(server)
    .get(url + '/users/getall')
    .end((err, res) => {
      if (err) {
        return;
      }
      res.should.have.status(200);
      done();
    });
});
