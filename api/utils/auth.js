'use strict';

const passport = require('passport');

module.exports.ensured = ensureAuth;
module.exports.bearer = bearerAuth;

function ensureAuth (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  res.status(401).json({ message: 'Unauthorized.' });
}

function bearerAuth (req, res, next) {
  return passport.authenticate('bearer', { session: false });
}
