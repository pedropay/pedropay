'use strict';

const winston = require('winston');
const config = require('../config');

let level = process.env.LOGGER_LEVEL || config.logger.level;

winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
  level: level,
  json: false,
  colorize: true
});

module.exports = winston;
