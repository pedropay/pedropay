const gulp = require('gulp');
const gulpSequence = require('gulp-sequence').use(gulp);
const bump = require('gulp-bump');
const git = require('gulp-git');
const argv = require('yargs').argv;
const fs = require('fs');
const semver = require('semver');

const getPackageJson = () => JSON.parse(fs.readFileSync('./package.json', 'utf8'));

gulp.task('default', () => {
	// place code for your default task here
});

gulp.task('bump', () => {
	const bumpOptions = {
		type: 'patch',
		preid: ''
	};

	if (argv.major) {
		bumpOptions.type = 'major';
	} else if (argv.minor) {
		bumpOptions.type = 'minor';
	} else if (argv.patch) {
		bumpOptions.type = 'patch';
	} else if (argv.prerelease) {
		bumpOptions.type = 'prerelease';
		bumpOptions.preid = argv.preid;
	}

	const pkg = getPackageJson();
	const newVer = semver.inc(pkg.version, bumpOptions.type, bumpOptions.preid);

	gulp.src('./package.json')
		.pipe(bump(bumpOptions))
		.pipe(gulp.dest('./'))
		.pipe(git.add())
		.pipe(git.commit(`Version bumped to ${newVer}`));
});

gulp.task('release', gulpSequence('bump'));
