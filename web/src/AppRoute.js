import React from 'react';
import DocumentTitle from 'react-document-title';
import { Route } from 'react-router-dom';

import PropTypes from 'prop-types';

import BlankLayout from '~modules/BlankLayout';
import { generateTitle } from '~utils/title';

const AppRoute = ({ component: Component, layout: Layout, title, ...rest }) => (
	<Route
		{...rest}
		render={props => (
			<DocumentTitle title={generateTitle({ title })}>
				<Layout>
					<Component {...props} />
				</Layout>
			</DocumentTitle>
		)}
	/>
);

AppRoute.propTypes = {
	component: PropTypes.func,
	layout: PropTypes.func,
	title: PropTypes.string
};

AppRoute.defaultProps = {
	component: null,
	layout: BlankLayout,
	title: null
};

export default AppRoute;
