/* eslint-disable import/prefer-default-export */
export const DATE_FORMAT = 'MM-DD-YYYY';
export const DATE_TIME_FORMAT = 'MM-DD-YYYY h:mm a';
export const CURRENCY_FORMAT = 'P 0,0.00';
export const PERCENTAGE_FORMAT = '0%';
