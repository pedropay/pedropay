import React from 'react';
import { render } from 'react-dom';
import Favicon from 'react-favicon';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

import BlankLayout from '~modules/BlankLayout';
import AuthorizedLayout from '~modules/AuthorizedLayout';

import Analytics from '~modules/analytics/Analytics';
import Login from '~modules/auth/Login';
import Home from '~modules/home/Home';
import Register from '~modules/register/Register';

import configureStore from './store/configureStore';
import AppRoute from './AppRoute';

import 'semantic-ui-css/semantic.min.css';

import 'babel-polyfill';
import 'ionicons/css/ionicons.min.css';
import 'react-table/react-table.css';
import 'react-vis/dist/style.css';
import 'toastr/build/toastr.min.css';

import '~assets/styl/main.styl';

const faviconPath = require(`~assets/img/favicon.ico`);

const store = configureStore();

render(
	<div>
		<Favicon url={faviconPath} />
		<Provider store={store}>
			<Router>
				<Switch>
					<AppRoute exact path="/register" title="Register" layout={BlankLayout} component={Register} />
					<AppRoute exact path="/home" title="Home" layout={AuthorizedLayout} component={Home} />
					<AppRoute exact path="/analytics" title="Analytics" layout={AuthorizedLayout} component={Analytics} />
					<AppRoute title="Login" layout={BlankLayout} component={Login} />
				</Switch>
			</Router>
		</Provider>
	</div>, document.getElementById('app')
);
