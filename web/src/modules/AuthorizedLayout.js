import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';

import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { Container, Dropdown, Menu } from 'semantic-ui-react';

import * as authActions from '~auth/auth.actions';
import auth from '~auth/auth.library';

class AuthorizedLayout extends Component {
	constructor(props, context) {
		super(props, context);

		this._handleLogout = this._handleLogout.bind(this);
	}

	componentWillMount() {
		const { getUser } = this.props;

		getUser();
	}

	_handleLogout() {
		const { history } = this.props;

		auth.clearToken();
		history.push('/login');
	}

	render() {
		const { children, currentUser } = this.props;

		return (
			<div>
				<Menu fixed="top" inverted color="green">
					<Menu.Item as="a" header>pedropay</Menu.Item>
					<Menu.Item as={NavLink} name="Home" to="/home" exact />
					<Menu.Item as={NavLink} name="Analytics" to="/analytics" exact />
					<Menu.Menu position="right">
						<Dropdown item simple text={currentUser.name}>
							<Dropdown.Menu>
								<Dropdown.Item>Account</Dropdown.Item>
								<Dropdown.Item onClick={this._handleLogout}>Logout</Dropdown.Item>
							</Dropdown.Menu>
						</Dropdown>
					</Menu.Menu>
				</Menu>
				<Container className="authorized">
					{children}
				</Container>
			</div>
		);
	}
}

AuthorizedLayout.propTypes = {
	children: PropTypes.element,
	currentUser: PropTypes.object.isRequired,
	getUser: PropTypes.func.isRequired,
	history: PropTypes.object.isRequired
};

AuthorizedLayout.defaultProps = {
	children: null
};

function mapStateToProps(state) {
	return {
		currentUser: state.currentUser
	};
}

function mapDispatchToProps(dispatch) {
	return {
		...bindActionCreators(authActions, dispatch)
	};
}

const enhance = compose(
	connect(mapStateToProps, mapDispatchToProps),
	withRouter
);

export default enhance(AuthorizedLayout);
