import React, { Component } from 'react';

import PropTypes from 'prop-types';

class BlankLayout extends Component {
	constructor(props, context) {
		super(props, context);
	}

	render() {
		const { children } = this.props;

		return (
			<div>
				{children}
			</div>
		);
	}
}

BlankLayout.propTypes = {
	children: PropTypes.element
};

BlankLayout.defaultProps = {
	children: null
};

export default BlankLayout;
