import React from 'react';
import { NavLink } from 'react-router-dom';

import PropTypes from 'prop-types';

const Header = () => (
	<div className="header">
		<div className="header__logo">
			<NavLink to="/" exact>
				<i className="ion-ios-bolt-outline" />
			</NavLink>
		</div>
	</div>
);

export default Header;
