import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
	HorizontalGridLines,
	LineSeries,
	VerticalGridLines,
	XAxis,
	XYPlot,
	YAxis
} from 'react-vis';

import moment from 'moment';
import { bindActionCreators } from 'redux';
import { Grid, Header, Segment } from 'semantic-ui-react';

class Analytics extends Component {
	constructor(props, context) {
		super(props, context);

		this.state = {};
	}

	render() {
		const lineData = [
			{ x: moment().subtract(9, 'd').valueOf(), y: 285 },
			{ x: moment().subtract(8, 'd').valueOf(), y: 409 },
			{ x: moment().subtract(7, 'd').valueOf(), y: 208 },
			{ x: moment().subtract(6, 'd').valueOf(), y: 287 },
			{ x: moment().subtract(5, 'd').valueOf(), y: 243 },
			{ x: moment().subtract(4, 'd').valueOf(), y: 130 },
			{ x: moment().subtract(3, 'd').valueOf(), y: 372 },
			{ x: moment().subtract(2, 'd').valueOf(), y: 420 },
			{ x: moment().subtract(1, 'd').valueOf(), y: 279 },
			{ x: moment().valueOf(), y: 192 }
		];

		return (
			<Grid padded stackable>
				<Grid.Row>
					<Grid.Column>
						<Segment>
							<Header as="h4" dividing>Sales Over Time</Header>
							<XYPlot height={300} width={600} xType="time">
								<VerticalGridLines />
								<HorizontalGridLines />
								<XAxis />
								<YAxis />
								<LineSeries data={lineData} />
							</XYPlot>
						</Segment>
					</Grid.Column>
				</Grid.Row>
			</Grid>
		);
	}
}

Analytics.propTypes = {

};

Analytics.defaultProps = {

};

Analytics.contextTypes = {

};

function mapStateToProps(state, ownProps) {
	return {
		transactions: state.transactions
	};
}

export default connect(mapStateToProps)(Analytics);
