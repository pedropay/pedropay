import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';

import axios from 'axios';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { Form, Grid, Header, Message, Segment } from 'semantic-ui-react';

import auth from '~auth/auth.library';
import * as types from '~const/actionTypes';
import * as api from '~const/api';
import * as facebook from '~const/facebook';
import notification from '~modules/notification/notification.library';

class Login extends Component {
	constructor(props, context) {
		super(props, context);

		this._handleFacebookResponse = this._handleFacebookResponse.bind(this);

		this.state = {};
	}

	componentWillMount() {
		const { history } = this.props;

		if (auth.isLoggedIn()) {
			history.push('/home');
		}
	}

	_handleFacebookResponse(response) {
		const { dispatch, history } = this.props;

		axios.get(`${api.URL}/users/find-by-fbid?fb_id=${response.id}`)
			.then(res => {
				const user = res.data.data;

				dispatch({ type: types.SET_USER_SUCCESS, data: user });
				auth.setToken(user.id);
				history.push('/home');
			}).catch(error => {
				notification.showError(error.response.data.message);
			});
	}

	render() {
		return (
			<div className="login-form">
				<Grid
					textAlign="center"
					style={{ height: '100%' }}
					verticalAlign="middle">
					<Grid.Column style={{ maxWidth: 450 }}>
						<Header as="h2" color="green" textAlign="center">
							Welcome to pedropay!
						</Header>
						<Form size="large">
							<Segment>
								<FacebookLogin
									appId={facebook.APP_ID}
									callback={this._handleFacebookResponse}
									fields={facebook.FIELDS}
									icon={facebook.ICON}
									textButton="Login using Facebook"
								/>
							</Segment>
						</Form>
						<Message>
						New to us? <NavLink to="/register" exact>Register</NavLink>
						</Message>
					</Grid.Column>
				</Grid>
			</div>
		);
	}
}

Login.propTypes = {
	dispatch: PropTypes.func.isRequired,
	history: PropTypes.object.isRequired
};

Login.defaultProps = {

};

Login.contextTypes = {

};

function mapDispatchToProps(dispatch) {
	return {
		dispatch
	};
}

const enhance = compose(
	connect(null, mapDispatchToProps),
	withRouter
);

export default enhance(Login);
