/* eslint-disable import/prefer-default-export */
import axios from 'axios';

import auth from '~auth/auth.library';
import * as types from '~const/actionTypes';
import * as api from '~const/api';

export function getUser() {
	return dispatch =>
		axios.get(`${api.URL}/users/${auth.getToken()}`)
			.then(res => {
				const user = res.data.data;

				dispatch({ type: types.SET_USER_SUCCESS, data: user });
			}).catch(error => {
				throw error;
			});
}

export function updateUser(data) {
	return dispatch =>
		axios.put(`${api.URL}/users/${auth.getToken()}`, data)
			.then(res => {
				const user = res.data.data;

				dispatch({ type: types.SET_USER_SUCCESS, data: user });
			}).catch(error => {
				throw error;
			});
}
