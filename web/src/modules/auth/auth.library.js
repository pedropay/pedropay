const guid = 'ff2764f5af5753c693214b20afdf84c0';

export default {
	isLoggedIn() {
		return !!localStorage[guid];
	},

	setToken(token) {
		if (typeof localStorage !== "undefined") {
			localStorage[guid] = token;
		}
	},

	getToken() {
		if (typeof localStorage !== "undefined") {
			return localStorage[guid];
		}
	},

	clearToken() {
		if (typeof localStorage !== "undefined") {
			localStorage.removeItem(guid);
		}
	}
};
