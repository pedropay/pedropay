import * as types from '~const/actionTypes';
import initialState from '~reducers/initialState';

export default (state = initialState.currentUser, action = {}) => {
	switch (action.type) {
		case types.SET_USER_SUCCESS: {
			return {
				...action.data
			};
		}

		default:
			return state;
	}
};
