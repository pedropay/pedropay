import React, { Component } from 'react';
import { connect } from 'react-redux';

import axios from 'axios';
import numeral from 'numeral';
import PropTypes from 'prop-types';
import {
	Grid,
	Header,
	Segment,
	Statistic
} from 'semantic-ui-react';

import * as api from '~const/api';
import * as formats from '~const/formats';
import notification from '~modules/notification/notification.library';
import TransactionHistory from '~modules/transactions/components/TransactionHistory';

class Home extends Component {
	constructor(props, context) {
		super(props, context);

		this._getBalance = this._getBalance.bind(this);

		this.state = {
			balance: 0
		};
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.currentUser) {
			this._getBalance(nextProps);
		}
	}

	_getBalance(props) {
		const { currentUser } = props;

		axios.get(`${api.URL}/ub/accountbal/${currentUser.uid}`)
			.then(res => {
				this.setState({ balance: res.data.data });
			}).catch(error => {
				notification.showError("Failed to retrieve balance.");
			});
	}

	render() {
		const { balance } = this.state;

		return (
			<Grid padded stackable>
				<Grid.Row>
					<Grid.Column width="4">
						<Segment>
							<Header as="h4" dividing>Your Account</Header>
							<Grid textAlign="center">
								<Statistic size="small">
									<Statistic.Label>Balance</Statistic.Label>
									<Statistic.Value>{numeral(balance).format(formats.CURRENCY_FORMAT)}</Statistic.Value>
								</Statistic>
							</Grid>
						</Segment>
					</Grid.Column>
					<Grid.Column width="12">
						<Segment>
							<Header as="h4" dividing>Recent Transactions</Header>
							<TransactionHistory />
						</Segment>
					</Grid.Column>
				</Grid.Row>
			</Grid>
		);
	}
}

Home.propTypes = {
	currentUser: PropTypes.object.isRequired
};

Home.defaultProps = {

};

Home.contextTypes = {

};

function mapStateToProps(state, ownProps) {
	return {
		currentUser: state.currentUser
	};
}

export default connect(mapStateToProps)(Home);
