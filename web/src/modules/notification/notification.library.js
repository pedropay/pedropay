import toastr from 'toastr';

import messages from './messages';

const toastrOptions = {
	closeButton: false,
	debug: false,
	newestOnTop: false,
	progressBar: false,
	positionClass: 'toast-top-right',
	preventDuplicates: false,
	onclick: null,
	showDuration: '300',
	hideDuration: '1000',
	timeOut: '5000',
	extendedTimeOut: '1000',
	showEasing: 'swing',
	hideEasing: 'linear',
	showMethod: 'fadeIn',
	hideMethod: 'fadeOut'
};

export default {
	showSuccess(message, title, options) {
		this.show(message, title, 'success', options);
	},

	showError(message, title, options) {
		this.show(message, title, 'error', options);
	},

	showWarning(message, title, options) {
		this.show(message, title, 'warning', options);
	},

	showInfo(message, title, options) {
		this.show(message, title, 'info', options);
	},

	show(message, title, type, userOptions) {
		const options = Object.assign({}, toastrOptions, userOptions);
		let retrievedMessage = null;

		if (message.code) {
			if (messages[message.code]) {
				retrievedMessage = messages[message.code];
			} else {
				retrievedMessage = message.message;
			}
		}

		switch (type) {
			case 'success':
				toastr.success(retrievedMessage || message, title || 'Success', options);

				break;
			case 'error':
				toastr.error(retrievedMessage || message, title || 'Error', options);

				break;
			case 'warning':
				toastr.warning(retrievedMessage || message, title || 'Warning', options);

				break;
			default:
				toastr.info(retrievedMessage || message, title || 'Info', options);

				break;
		}
	}
};
