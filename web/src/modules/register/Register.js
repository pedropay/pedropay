import React, { Component } from 'react';
import { withRouter } from 'react-router';

import PropTypes from 'prop-types';
import {
	Container,
	Grid,
	Header,
	Icon,
	Step
} from 'semantic-ui-react';

import auth from '~auth/auth.library';

import ConfirmInformation from './components/ConfirmInformation';
import Facebook from './components/Facebook';
import UnionBank from './components/UnionBank';

class Register extends Component {
	constructor(props, context) {
		super(props, context);

		this._nextStep = this._nextStep.bind(this);

		this.state = {
			activeStep: 0,
			steps: [
				Facebook,
				UnionBank,
				ConfirmInformation
			]
		};
	}

	componentWillMount() {
		const { history, location } = this.props;
		const { steps } = this.state;

		if (auth.isLoggedIn()) {
			if (location.search === '?ub_auth=1') {
				this.setState({ activeStep: steps.length - 1 });
			} else {
				history.push('/home');
			}
		}
	}

	_nextStep() {
		const { history } = this.props;
		const { activeStep, steps } = this.state;

		if (activeStep === steps.length - 1) {
			history.push('/home');
		} else {
			this.setState({ activeStep: activeStep + 1 });
		}
	}

	render() {
		const { activeStep, steps } = this.state;
		const ActiveSegment = steps[activeStep];

		return (
			<Container>
				<Grid padded>
					<Grid.Row>
						<Grid.Column>
							<Header>pedropay</Header>
							<Step.Group
								attached="top"
								fluid>
								<Step active={activeStep === 0} completed={activeStep > 0}>
									<Icon name="facebook official" />
									<Step.Content>
										<Step.Title>Facebook</Step.Title>
										<Step.Description>Connect your Facebook account</Step.Description>
									</Step.Content>
								</Step>

								<Step active={activeStep === 1} completed={activeStep > 1}>
									<Icon name="money" />
									<Step.Content>
										<Step.Title>UnionBank</Step.Title>
										<Step.Description>Connect your UnionBank account</Step.Description>
									</Step.Content>
								</Step>

								<Step active={activeStep === 2} completed={activeStep > 2}>
									<Icon name="star" />
									<Step.Content>
										<Step.Title>Confirm Registration</Step.Title>
										<Step.Description>Review the information you provided</Step.Description>
									</Step.Content>
								</Step>
							</Step.Group>
							<ActiveSegment onComplete={this._nextStep} />
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</Container>
		);
	}
}

Register.propTypes = {
	history: PropTypes.object.isRequired,
	location: PropTypes.object.isRequired
};

Register.defaultProps = {

};

Register.contextTypes = {

};

export default withRouter(Register);
