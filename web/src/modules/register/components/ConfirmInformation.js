import React, { Component } from 'react';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {
	Button,
	Form,
	Grid,
	Header,
	Input,
	Segment
} from 'semantic-ui-react';

import * as authActions from '~auth/auth.actions';
import notification from '~modules/notification/notification.library';

class ConfirmInformation extends Component {
	constructor(props, context) {
		super(props, context);

		this._handleChange = this._handleChange.bind(this);
		this._submit = this._submit.bind(this);

		this.state = {
			name: '',
			email: '',
			pin: '',
			confirmPin: ''
		};
	}

	componentWillMount() {
		const { getUser } = this.props;

		getUser();
	}

	componentWillReceiveProps(nextProps) {
		this.setState(nextProps.currentUser);
	}

	_handleChange(key, value) {
		this.setState({ [key]: value });
	}

	_submit() {
		const { onComplete, updateUser } = this.props;
		const { confirmPin, pin } = this.state;

		if (pin !== confirmPin) {
			notification.showError('PIN does not match.');

			return this.setState({
				pin: '',
				confirmPin: ''
			});
		}

		updateUser(this.state)
			.then(res => {
				onComplete();
			})
			.catch(error => {
				notification.showError(error.response.data.message);
			});
	}

	render() {
		const { confirmPin, email, name, pin } = this.state;

		return (
			<Segment attached>
				<Form>
					<Header as="h4" dividing>Personal Information</Header>
					<Form.Group inline>
						<label className="four wide field text-right" htmlFor="name">Name</label>
						<Form.Field width="8">
							<Input
								onChange={e => this._handleChange('name', e.target.value)}
								value={name}
							/>
						</Form.Field>
					</Form.Group>
					<Form.Group inline>
						<label className="four wide field text-right" htmlFor="name">Email Address</label>
						<Form.Field width="8">
							<Input
								onChange={e => this._handleChange('email', e.target.value)}
								value={email}
							/>
						</Form.Field>
					</Form.Group>
					<Header as="h4" dividing>Account Security</Header>
					<Form.Group inline>
						<label className="four wide field text-right" htmlFor="name">PIN</label>
						<Form.Field width="4">
							<Input
								onChange={e => this._handleChange('pin', e.target.value)}
								value={pin}
							/>
						</Form.Field>
					</Form.Group>
					<Form.Group inline>
						<label className="four wide field text-right" htmlFor="name">Confirm PIN</label>
						<Form.Field width="4">
							<Input
								onChange={e => this._handleChange('confirmPin', e.target.value)}
								value={confirmPin}
							/>
						</Form.Field>
					</Form.Group>
					<Grid>
						<Grid.Row>
							<Grid.Column textAlign="right">
								<Button primary onClick={this._submit}>Finish</Button>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Form>
			</Segment>
		);
	}
}

ConfirmInformation.propTypes = {
	currentUser: PropTypes.object.isRequired,
	getUser: PropTypes.func.isRequired,
	onComplete: PropTypes.func,
	updateUser: PropTypes.func.isRequired
};

ConfirmInformation.defaultProps = {
	onComplete: () => {}
};

ConfirmInformation.contextTypes = {

};

function mapStateToProps(state) {
	return {
		currentUser: state.currentUser
	};
}

function mapDispatchToProps(dispatch) {
	return {
		...bindActionCreators(authActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmInformation);
