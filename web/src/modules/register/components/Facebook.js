import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login';

import axios from 'axios';
import PropTypes from 'prop-types';
import {
	Button,
	Grid,
	Segment
} from 'semantic-ui-react';

import auth from '~auth/auth.library';
import * as api from '~const/api';
import * as facebook from '~const/facebook';
import notification from '~modules/notification/notification.library';

class Facebook extends Component {
	constructor(props, context) {
		super(props, context);

		this._handleFacebookResponse = this._handleFacebookResponse.bind(this);

		this.state = {};
	}

	_handleFacebookResponse(response) {
		const { onComplete } = this.props;
		const payload = {
			email: response.email,
			name: response.name,
			fb_id: response.id,
			fb_access_token: response.accessToken,
			fb_token_expires: response.expiresIn
		};

		axios.post(`${api.URL}/users`, payload)
			.then(res => {
				const user = res.data.data;

				if (user.is_new) {
					auth.setToken(user.id);
					onComplete();
				}
			}).catch(error => {
				notification.showError(error.response.data.message);
			});
	}

	render() {
		const { onComplete } = this.props;

		return (
			<Segment attached>
				<Grid>
					<Grid.Row>
						<Grid.Column textAlign="center">
							<FacebookLogin
								appId={facebook.APP_ID}
								callback={this._handleFacebookResponse}
								fields={facebook.FIELDS}
								icon={facebook.ICON}
								textButton="Register using Facebook"
							/>
						</Grid.Column>
					</Grid.Row>
					<Grid.Row>
						<Grid.Column textAlign="right">
							<Button onClick={onComplete}>Next</Button>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</Segment>
		);
	}
}

Facebook.propTypes = {
	onComplete: PropTypes.func
};

Facebook.defaultProps = {
	onComplete: () => {}
};

Facebook.contextTypes = {

};

export default Facebook;
