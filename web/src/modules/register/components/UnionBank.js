import React, { Component } from 'react';

import PropTypes from 'prop-types';
import {
	Button,
	Grid,
	Segment
} from 'semantic-ui-react';

import auth from '~auth/auth.library';
import * as api from '~const/api';

class UnionBank extends Component {
	constructor(props, context) {
		super(props, context);

		this._redirectToUBAuth = this._redirectToUBAuth.bind(this);

		this.state = {};
	}

	_redirectToUBAuth() {
		window.location = `${api.URL}/ub/init/${auth.getToken()}`;
	}

	render() {
		return (
			<Segment attached>
				<Grid>
					<Grid.Row>
						<Grid.Column textAlign="center">
							<Button onClick={this._redirectToUBAuth} size="large" color="orange">Connect your UnionBank account</Button>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</Segment>
		);
	}
}

UnionBank.propTypes = {

};

UnionBank.defaultProps = {

};

UnionBank.contextTypes = {

};

export default UnionBank;
