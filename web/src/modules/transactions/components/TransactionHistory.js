import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';

import moment from 'moment';
import numeral from 'numeral';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

import * as formats from '~const/formats';

import * as transactionActions from '../transaction.actions';

class TransactionHistory extends Component {
	constructor(props, context) {
		super(props, context);

		this.state = {};
	}

	componentWillMount() {
		const { loadTransactions } = this.props;

		loadTransactions();
	}

	_generateColumns() {
		return [
			{
				Header: 'Date',
				accessor: 'created_at',
				Cell: row => (
					<span>{moment(row.value).format(formats.DATE_TIME_FORMAT)}</span>
				),
				maxWidth: 150
			},
			{
				Header: 'Transaction No.',
				accessor: 'uid'
			},
			{
				Header: 'Sender',
				accessor: 'sender_name'
			},
			{
				Header: 'Receiver',
				accessor: 'receiver_name'
			},
			{
				Header: 'Amount',
				accessor: 'amount',
				Cell: row => (
					<div style={{ textAlign: 'right' }}>{numeral(row.value).format(formats.CURRENCY_FORMAT)}</div>
				),
				maxWidth: 100
			}
		];
	}

	render() {
		const { transactions } = this.props;

		return (
			<div>
				<ReactTable
					columns={this._generateColumns()}
					data={transactions}
					defaultPageSize={10}
					defaultSorted={[
						{
							id: 'created_at',
							desc: true
						}
					]}
				/>
			</div>
		);
	}
}

TransactionHistory.propTypes = {
	loadTransactions: PropTypes.func.isRequired,
	transactions: PropTypes.arrayOf(PropTypes.object).isRequired
};

TransactionHistory.defaultProps = {

};

TransactionHistory.contextTypes = {

};

function mapStateToProps(state, ownProps) {
	return {
		transactions: state.transactions
	};
}

function mapDispatchToProps(dispatch) {
	return {
		...bindActionCreators(transactionActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionHistory);
