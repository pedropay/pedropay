/* eslint-disable import/prefer-default-export */
import axios from 'axios';

import * as types from '~const/actionTypes';
import * as api from '~const/api';

export function loadTransactions() {
	return dispatch =>
		axios.get(`${api.URL}/transactions/user/11`)
			.then(res => {
				dispatch(loadTransactionsSuccess(res.data));
			}).catch(error => {
				throw error;
			});
}

export function loadTransactionsSuccess(res) {
	return { type: types.LOAD_TRANSACTIONS_SUCCESS, data: res.data };
}
