import * as types from '~const/actionTypes';
import initialState from '~reducers/initialState';

export default (state = initialState.transactions, action = {}) => {
	switch (action.type) {
		case types.LOAD_TRANSACTIONS_SUCCESS: {
			return [
				...action.data
			];
		}

		default:
			return state;
	}
};
