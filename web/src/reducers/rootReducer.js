import { combineReducers } from 'redux';

import currentUser from '../modules/auth/auth.reducer';
import transactions from '../modules/transactions/transaction.reducer';

const rootReducer = combineReducers({
	currentUser,
	transactions
});

export default rootReducer;
