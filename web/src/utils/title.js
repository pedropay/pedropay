/* eslint-disable import/prefer-default-export */
import * as strings from '~const/strings';

export function generateTitle(props, append) {
	let title = strings.APP_TITLE;

	if (props && props.title) {
		title += ` - ${props.title}`;
	}

	if (append) {
		title += ` - ${append}`;
	}

	return title;
}
