import webpack from 'webpack';
import path from 'path';
import poststylus from 'poststylus';
import autoprefixer from 'autoprefixer';
import DotenvPlugin from 'webpack-dotenv-plugin';

export default {
	devtool: 'cheap-module-eval-source-map',
	entry: [
		'eventsource-polyfill',
		'react-hot-loader/patch',
		'webpack-hot-middleware/client?reload=true',
		'./src/index'
	],
	target: 'web',
	output: {
		path: path.resolve(__dirname, 'dist'),
		publicPath: '/',
		filename: 'bundle.js'
	},
	devServer: {
		contentBase: path.join(__dirname, 'src'),
		debug: true,
		hot: true,
		noInfo: true
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.optimize.ModuleConcatenationPlugin(),
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery"
		}),
		new DotenvPlugin({
			sample: './.env.sample',
			path: './.env'
		})
	],
	resolve: {
		modules: ['node_modules', path.resolve(__dirname, 'src')],
		extensions: ['.js', '.json', '.jsx', '.css', '.styl'],
		alias: {
			'~assets': path.join(__dirname, 'src/assets'),
			'~auth': path.join(__dirname, 'src/modules/auth'),
			'~bower': path.join(__dirname, 'bower_components'),
			'~const': path.join(__dirname, 'src/constants'),
			'~global': path.join(__dirname, 'src/modules/_global'),
			'~modules': path.join(__dirname, 'src/modules'),
			'~reducers': path.join(__dirname, 'src/reducers'),
			'~utils': path.join(__dirname, 'src/utils')
		}
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader'
				}
			},
			{
				test: /(\.css)$/,
				use: [
					'style-loader',
					'css-loader'
				]
			},
			{
				test: /\.styl$/,
				use: [
					'style-loader',
					'css-loader',
					{
						loader: 'stylus-loader',
						options: {
							use: [poststylus([
								autoprefixer({ browsers: ['last 2 version', '> 1%', 'IE > 8'] })
							])]
						}
					}
				]
			},
			{
				test: /\.(png|jpe?g|gif|ico)$/,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: 100000,
							name: '[name]-[hash:6].[ext]'
						}
					}
				]
			},
			{
				test: /\.(woff|woff2|svg|ttf|eot|otf)(\?[\s\S]+)?$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							limit: 100000,
							name: '[name].[ext]'
						}
					}
				]
			},
			{
				test: /\.json$/,
				loader: 'json-loader'
			}
		]
	}
};
