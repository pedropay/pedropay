import webpack from 'webpack';
import path from 'path';
import poststylus from 'poststylus';
import autoprefixer from 'autoprefixer';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import DotenvPlugin from 'webpack-dotenv-plugin';
import chalk from 'chalk';
import ProgressBarPlugin from 'progress-bar-webpack-plugin';

const GLOBALS = {
	'process.env.NODE_ENV': JSON.stringify('production')
};

export default {
	devtool: 'source-map',
	entry: './src/index',
	target: 'web',
	output: {
		path: path.join(__dirname, '/temp-dist'),
		publicPath: '/',
		filename: 'assets/js/bundle.js'
	},
	devServer: {
		contentBase: './dist'
	},
	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.optimize.ModuleConcatenationPlugin(),
		new webpack.LoaderOptionsPlugin({
			debug: true,
			noInfo: false
		}),
		new webpack.DefinePlugin(GLOBALS),
		new ExtractTextPlugin('assets/css/styles.css'),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.UglifyJsPlugin(),
		new ProgressBarPlugin({
			format: `Status [:bar] ${chalk.green.bold(':percent')} :msg (:elapsed seconds)`,
			clear: false
		}),
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery"
		}),
		new DotenvPlugin({
			sample: './.env.sample',
			path: './.env'
		})
	],
	resolve: {
		modules: ['node_modules', path.resolve(__dirname)],
		extensions: ['.js', '.json', '.jsx', '.css', '.styl'],
		alias: {
			'~assets': path.join(__dirname, 'src/assets'),
			'~auth': path.join(__dirname, 'src/modules/auth'),
			'~bower': path.join(__dirname, 'bower_components'),
			'~const': path.join(__dirname, 'src/constants'),
			'~global': path.join(__dirname, 'src/modules/_global'),
			'~modules': path.join(__dirname, 'src/modules'),
			'~reducers': path.join(__dirname, 'src/reducers'),
			'~utils': path.join(__dirname, 'src/utils')
		}
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader'
				}
			},
			{
				test: /(\.css)$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: 'css-loader?sourceMap'
				})
			},
			{
				test: /\.styl$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						'css-loader',
						{
							loader: 'stylus-loader',
							options: {
								use: [poststylus([
									autoprefixer({ browsers: ['last 2 version', '> 1%', 'IE > 8'] })
								])]
							}
						}
					]
				})
			},
			{
				test: /\.(png|jpe?g|gif|ico)$/,
				use: {
					loader: 'url-loader',
					options: {
						limit: 100000,
						name: 'assets/img/[name]-[hash:6].[ext]'
					}
				}
			},
			{
				test: /\.(otf|woff|woff2|svg|ttf|eot)(\?[\s\S]+)?$/,
				use: {
					loader: 'file-loader',
					options: {
						name: 'assets/fonts/[name].[ext]'
					}
				}
			},
			{
				test: /\.json$/,
				use: 'json-loader'
			}
		]
	}
};
